#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" generate forms for statutory declarations for our exams using templates """

import yaml
import jinja2
import os, sys

class Declaration():

    template = r"""
% -*- mode: LaTeX; TeX-engine: xetex -*-
\documentclass[de-DE]{declaration}

\newcommand{\VAR}[1]{}
\newcommand{\BLOCK}[1]{1}

\module{\VAR{module.title}}
\day=\VAR{examination.date.strftime('%d')}
\month=\VAR{examination.date.strftime('%m')}
\year=\VAR{examination.date.strftime('%Y')}

\BLOCK{ for aid in examination.aids }
\aid{\VAR{aid}}
\BLOCK{ endfor }

\begin{document}
  \input{templates/DE}
\end{document}
"""
    
    def __init__(self, yfile=None, template='template.tex'):
        self.basename, _ = os.path.splitext(yfile)
        self.texed = False

        self.get_template(template)
        self.get_yaml()

    def get_template(self, tfile):
        latex_jinja_env = jinja2.Environment(
            block_start_string = '\BLOCK{',
            block_end_string = '}',
            variable_start_string = '\VAR{',
            variable_end_string = '}',
            comment_start_string = '\#{',
            comment_end_string = '}',
            line_statement_prefix = '%-',
            line_comment_prefix = '%#',
            trim_blocks = True,
            autoescape = False,
            loader = jinja2.FileSystemLoader(os.path.abspath('.'))
        )
        self.template = latex_jinja_env.from_string(self.template)

    def get_yaml(self):
        if not self.basename:
            self.data = None
        with open(self.basename + ".yaml", 'r') as stream:
            self.data = yaml.safe_load(stream)

    def create_TeX(self):
        with open(self.basename + ".tex", 'w') as tex:
            tex.write( self.template.render(**self.data) )
        self.texed = True

    def create_PDF(self):
        if not self.texed:
            self.create_TeX()
        os.system(f"xelatex -interaction=batchmode {self.basename}")

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(f'usage: {sys.argv[0]} exam.yaml [exam.yaml [exam.yaml ...]]')

    for arg in sys.argv[1:]:
        form = Declaration(yfile=arg)
        form.create_PDF()
        
